# AI_Server 
Apache License 2.0 开源免费商业

#### 如果对您有帮助，您可以点右上角 "Star" 支持一下 谢谢！

![输入图片说明](image.png)

![输入图片说明](code/AI_Server/image.png)


### 开源不易
大家有项目给我们开源团队做吗？<br>
群名称：开源企业合作<br>
群号：678199313<br>

# JsonSqlV3.0
介绍JsonSql，可以处理对象、Json、数组的筛选、统计、分组等功能，支持方法或sql语句。<br>
https://gitee.com/JeeHuangBingGui/JsonSql<br>
官方QQ群 798247492 <br>
https://jq.qq.com/?_wv=1027&k=5SLgtsI<br>

# AI_ServerV2.0
提供AI_Person_Server.exe直接运行<br>
提供HTTP接口服务<br>
行人识别和分析HTTP服务端等<br>
https://gitee.com/JeeHuangBingGui/AI_Server<br>

# OpenSoftFace
开源人脸考勤系统<br>
https://gitee.com/JeeHuangBingGui/OpenSoftFace

# FastBigData
敏捷大数据研发的分布式计算平台<br>
https://gitee.com/JeeHuangBingGui/FastBigDate

# 提供AI_Person_Server.exe直接运行；

# 提供HTTP接口服务；

# 行人识别、行人属性和分析HTTP服务端等等

![输入图片说明](./AI_Person_Server.jpg "在这里输入图片标题")

# 云盘下载
链接：https://pan.baidu.com/s/16JUIim0IHfvhNlrHsUSPjA 
提取码：1234
![输入图片说明](./AI_Person_ServerV2.0.jpg "在这里输入图片标题")

# 代码结构

![输入图片说明](https://images.gitee.com/uploads/images/2020/0828/175834_e99fcbe6_132236.png "code.png")
 
# 行人属性识别
![输入图片说明](http://5b0988e595225.cdn.sohucs.com/images/20190726/329137f37fe5404f9b5c2ef3cc35ae8a.jpeg "在这里输入图片标题")

![输入图片说明](http://5b0988e595225.cdn.sohucs.com/images/20190726/375f792bd2f14654adfd76e2f608c1b9.jpeg "在这里输入图片标题")

# 摄像头：
![输入图片说明](http://5b0988e595225.cdn.sohucs.com/images/20190726/8a8e9638ee9246b7949118b5e43ff5b3.jpeg "在这里输入图片标题")

# 请作者喝杯茶。(开源不易！)


# 官方QQ群：
![输入图片说明](./AIServer群二维码.png "在这里输入图片标题")

AIServer 1151642518

# 企业合作
群名称：开源企业合作

群   号：678199313